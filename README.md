# FEniCS Tutorial - Helmholtz Equation 1

First FEniCS tutorial for the Helmholtz equation.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Intro to FEniCS - Part 3]().

## Study Summary

The main parameters of the study are reported below.

### Source and Medium

The "source" is a uniform velocity Neumann boundary condition applied to the $`y=0`$ wall.

| Parameter Name             | Symbol   | Value | Unit                      |
|----------------------------|----------|-------|---------------------------|
| Source Frequencies         | $`f`$    | 57.17 | hertz                     |
| Source Surface Velocity    | $`w`$    | 10    | meters per second         |
| Medium Sound Phase Speed   | $`c`$    | 343   | meters per second         |
| Medium Equilibrium Density | $`\rho`$ | 1.205 | kilograms per cubic meter |

### Domain

#### Elmer

| Shape            | Size             | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size  | Element Order | Source Location |
|------------------|------------------|-----------------|----------------|-----------------|---------------|-----------------|
| Rectangular Room | 5 X 4 X 3 meters | NETGEN 1D-2D-3D | 50 millimetre  | 500 millimetres | Second        | $`y=0`$ wall    |

#### FEniCS

| Shape            | Size             | Mesh Algorithm  | Mesh Min. Size | Mesh Max. Size    | Element Order | Source Location |
|------------------|------------------|-----------------|----------------|-------------------|---------------|-----------------|
| Rectangular Room | 5 X 4 X 3 meters | NETGEN 1D-2D-3D | -              | $`\frac{f}{10c}`$ | Second        | $`y=0`$ wall    |

### Boundary Conditions

Uniform velocity on the $`y=0`$ wall. Remaining walls are rigid.

## Software Overview

The table below reports the software used for this project.

| Software                                                 | Usage                        |
|----------------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/) 0.19              | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) 9.7.0 | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)  8.4                 | Multiphysical solver         |
| [ParaView](https://www.paraview.org/) 5.10.0-RC1         | Post-processing              |
| [Python](https://www.python.org/) 3.6.9                  | Technical Computing Language |

For installation tips, see [Setting Up an Ubuntu Modelling Environment](https://computational-acoustics.gitlab.io/website/posts/1-setting-up-an-ubuntu-modelling-environment/). Note that ParaView can also be downloaded directly from its website. To install FEniCS, see [Intro to FEniCS - Part 1](https://computational-acoustics.gitlab.io/website/posts/30-intro-to-fenics-part-1/).

If you wish to create a virtual environment with a working FEniCS installation use the following:

```bash
python3 -m venv --system-site-packages venv
```

## Repo Structure

* `elmerfem` contains the Elmer simulation.
* `simulation` contains a basic Python package to implement the FEniCS simulation.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. Note that the mesh in this file is **not** computed.
* `run.py` is a minimal Python script tu run the FEniCS simulation.
* `run.sh` is a bash script to run both Elmer and FEniCS simulations.
* The `*.pvsm` files are ParaView state files. They can be loaded into ParaView to plot the solution.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/fenics-tutorial-helmholtz-equation-1.git
cd fenics-tutorial-helmholtz-equation-1/
```

Then, run the running script:

```bash
./run.sh
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` files in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)
