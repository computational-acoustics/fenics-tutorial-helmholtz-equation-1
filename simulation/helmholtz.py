import numpy as np
import fenics
import pathlib

# Room Acoustics: Helmholtz Equation in a room


def simulate(
        real_part_output_path: pathlib.Path,
        imag_part_output_path: pathlib.Path
) -> None:

    nu = 57.17  # Frequency of the Simulation, Hz
    c = 343  # Speed of sound in air, m/s
    rho = 1.205  # Density of air, kg/m^3
    w = 10  # Velocity normal to boundary, for inflow Neumann condition, m/s
    l_x0 = 4  # Room size along x0, m
    l_x1 = 5  # Room size along x1, m
    l_x2 = 3  # Room size along x2, m
    tol = 1e-10 # Tolerance for boundary condition definitions

    # Computing some useful stuff
    omega = 2 * np.pi * nu  # Angular frequency, rad/s
    k = omega / c  # Wave number, rad/m
    s = c / (10 * nu)  # Element Size
    n_x0 = np.intp(np.ceil(l_x0 / s))  # Number of elements for each direction
    n_x1 = np.intp(np.ceil(l_x1 / s))
    n_x2 = np.intp(np.ceil(l_x2 / s))

    # Making a mesh, then an element. Then, Mixed Space so that we can solve for real and imaginary parts.
    mesh = fenics.BoxMesh(
        fenics.Point(0, 0, 0),
        fenics.Point(l_x0, l_x1, l_x2),
        n_x0,
        n_x1,
        n_x2
    )

    P = fenics.FiniteElement("Lagrange", mesh.ufl_cell(), 2)
    V = fenics.FunctionSpace(mesh, P * P)

    # I also tried this, but same results:
    # element = fenics.MixedElement([P, P])
    # V = fenics.FunctionSpace(mesh, element)

    # Define variational problem
    u_re, u_im = fenics.TrialFunctions(V)
    phi_re, phi_im = fenics.TestFunctions(V)
    k_sq = fenics.Constant(k**2)

    # # As far as I understand this is the correct bilinear form. Right?
    # a = \
    #     fenics.inner(fenics.nabla_grad(u_re), fenics.nabla_grad(phi_re)) * fenics.dx - k_sq * phi_re * u_re * fenics.dx + \
    #     fenics.inner(fenics.nabla_grad(u_im), fenics.nabla_grad(phi_im)) * fenics.dx - k_sq * phi_im * u_im * fenics.dx

    a = \
        fenics.inner(fenics.nabla_grad(u_re), fenics.nabla_grad(phi_re)) * fenics.dx - k_sq * phi_re * u_re * fenics.dx + \
        fenics.inner(fenics.nabla_grad(u_im), fenics.nabla_grad(phi_im)) * fenics.dx - k_sq * phi_im * u_im * fenics.dx + \
        fenics.inner(fenics.nabla_grad(u_im), fenics.nabla_grad(phi_re)) * fenics.dx - k_sq * u_im * phi_re * fenics.dx - \
        fenics.inner(fenics.nabla_grad(u_re), fenics.nabla_grad(phi_im)) * fenics.dx + k_sq * u_re * phi_im * fenics.dx


    # Now I want to be able to use a different Neumann condition on each wall (even though g will be 0 on all walls aside
    # for one...)
    mf = fenics.MeshFunction("size_t", mesh, 2)
    mf.set_all(0)


    class BX0(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[0], 0, tol)


    class BXL(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[0], l_x0, tol)


    class BY0(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[1], 0, tol)


    class BYL(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[1], l_x1, tol)


    class BZ0(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[2], 0, tol)


    class BZL(fenics.SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and fenics.near(x[2], l_x2, tol)


    bx0 = BX0()
    bx0.mark(mf, 1)

    bxl = BXL()
    bxl.mark(mf, 2)

    by0 = BY0()
    by0.mark(mf, 3)

    byl = BYL()
    byl.mark(mf, 4)

    bz0 = BZ0()
    bz0.mark(mf, 5)

    bzl = BZL()
    bzl.mark(mf, 6)

    # The flux is 0 for all rigid walls
    flux_rig = 0
    g_rig_re = fenics.Constant(np.real(flux_rig))
    g_rig_im = fenics.Constant(np.imag(flux_rig))

    # The flux is this for the active walls
    flux_in = 1j * omega * rho * w
    g_in_re = fenics.Constant(np.real(flux_in))
    g_in_im = fenics.Constant(np.imag(flux_in))

    ds = fenics.Measure('ds', domain=mesh, subdomain_data=mf)

    # This produces a solution which has null imaginary part and null real part. Why?
    # L = \
    #     g_rig_re * phi_re * ds(1) + g_rig_im * phi_im * ds(1) + \
    #     g_rig_re * phi_re * ds(2) + g_rig_im * phi_im * ds(2) + \
    #     g_rig_re * phi_re * ds(3) + g_rig_im * phi_im * ds(3) + \
    #     g_rig_re * phi_re * ds(4) + g_rig_im * phi_im * ds(4) + \
    #     g_rig_re * phi_re * ds(5) + g_rig_im * phi_im * ds(5) + \
    #     g_in_re * phi_re * ds(6) + g_in_im * phi_im * ds(6)
    L = \
        (g_rig_re * phi_re + g_rig_im * phi_im + g_rig_im * phi_re - g_rig_re * phi_im) * ds(1) + \
        (g_rig_re * phi_re + g_rig_im * phi_im + g_rig_im * phi_re - g_rig_re * phi_im) * ds(2) + \
        (g_rig_re * phi_re + g_rig_im * phi_im + g_rig_im * phi_re - g_rig_re * phi_im) * ds(6) + \
        (g_rig_re * phi_re + g_rig_im * phi_im + g_rig_im * phi_re - g_rig_re * phi_im) * ds(4) + \
        (g_rig_re * phi_re + g_rig_im * phi_im + g_rig_im * phi_re - g_rig_re * phi_im) * ds(5) + \
        (g_in_re * phi_re + g_in_im * phi_im + g_in_im * phi_re - g_in_re * phi_im) * ds(3)

    # Tried this. Of course it makes all walls having the influx boundary condition. The real part makes kind of sense,
    # but the imaginary part is null. Why?
    # L = g_in_re * phi_re * fenics.ds + g_in_im * phi_im * fenics.ds

    # Compute solution
    u = fenics.Function(V)
    fenics.solve(a == L, u)

    # Save solution to file in VTK format
    u_1, u_2 = u.split()
    u_1.rename('re', 're')
    u_2.rename('im', 'im')

    vtk_file_1 = fenics.File(real_part_output_path.absolute().as_posix())
    vtk_file_1 << u_1

    vtk_file_2 = fenics.File(imag_part_output_path.absolute().as_posix())
    vtk_file_2 << u_2
