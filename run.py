from simulation import helmholtz
import pathlib

if __name__ == "__main__":
    helmholtz.simulate(
       real_part_output_path=pathlib.Path(__file__).parent.joinpath('results/solution_re.pvd'),
       imag_part_output_path=pathlib.Path(__file__).parent.joinpath('results/solution_im.pvd')
    )
